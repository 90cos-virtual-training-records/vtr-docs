### Feature
As a **____** I want to **____** so that **____**.

### Scenario 
Some situation
- **Given** Some condition
    - More conditions
- **When** The user does some action
    - More actions
- **Then** desired outcome happens
    - More happenings

### Scenario 
Additional scenario using format of the above scenario

/label ~"USER STORY" ~"backlog::estimate" ~"office::CYT"  
/milestone %"CYT Backlog"

#### Once Estimated
- [ ] `/estimate`
- [ ] `/weight`