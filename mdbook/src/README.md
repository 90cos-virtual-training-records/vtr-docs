# Virtual Training Records

### The Problem
How do we track training records for each individual? How do we track the member's progress? How do we document all training and evaluation material used by the member to meet requirements? Where do we store all this information?  How do we give credit across tasks when the are already accomplished?

Most importantly:  
How do we **MAKE IT EASY**?!

### The Solution
Use Gitlab issues to track and store all Virtual Training task progress in the same fashion as we accomplish our mission (there's a saying like 'train like you fight' to be inserted here). Gitlab can provide us with the infrastructure to track, store, maintain, and link all training information and Virtual Training progress for the member.

The use of the Virtual Training Records will be explained in further depth under each section of this mdbook, along with accompanying videos.