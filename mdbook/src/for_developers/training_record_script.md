## [training_record.py](training_record.py) script
### Help Menu
```sh
usage: training_record.py [-h] {create} ...

entrance script to create ksat/rel-link data

positional arguments:
  {create}    all Commands you can run
  args        arguments for command

optional arguments:
  -h, --help  show this help message and exit
```
### Description
This script acts as a points of entry to scripts that will do the work. This script will perform the following in this order.
1. Clone the MTTL repo if it has not already been cloned, else update.
2. Stage the mongo database with the latest MTTL data. 
3. Execute script that will do the desired work
## Commands
### Create
```sh
usage: create.py [-h] [-s SUBGROUP_NAME] [-p PROJECT_ID] [-n RANK FIRSTNAME LASTNAME] [-w {PO,SPO,SM,SSM,Agile Coach,CCD,SCCD-L,SCCD-W,MCCD,Special Missions,Sysadmin,TAE,JSRE,SSRE}] token

create training repo for 90COS members

positional arguments:
  token                 gitlab private token of account with 90COS access to member training repo group

optional arguments:
  -h, --help            show this help message and exit
  -s SUBGROUP_NAME, --subgroup-name SUBGROUP_NAME
                        specify case sensitive subgroup name that you want to create the VTR
  -p PROJECT_ID, --project-id PROJECT_ID
                        specify project-id for the member training repo project. only needed if not creating new training repo
  -n RANK FIRSTNAME LASTNAME, --new-training-repo RANK FIRSTNAME LASTNAME
                        takes three args; RANK, FIRSTNAME, and LASTNAME (if rank has spaces wrap in "")
  -w {PO,SPO,SM,SSM,Agile Coach,CCD,SCCD-L,SCCD-W,MCCD,Special Missions,Sysadmin,TAE,JSRE,SSRE}, --ojt-work-role {PO,SPO,SM,SSM,Agile Coach,CCD,SCCD-L,SCCD-W,MCCD,Special Missions,Sysadmin,TAE,JSRE,SSRE}
                        indicate the ojt work-role to import KSAT requirements into gitlab issues
```
#### Create Examples

Create a training record repo. This will not initialize the work-role tickets
```sh
./training_record.py create -n Amn John Doe [GitLab ACCESS TOKEN]
```

Initialize existing training record repo with work-role after it has been created
```sh
./training_record.py create -p 0000000 -w CCD [GitLab ACCESS TOKEN]
```

Create and initialize a training record repo inside the CYT subgroup
```sh
./training_record.py create -s CYT -n Amn John Doe -w CCD [GitLab ACCESS TOKEN]
```

### Update
```sh
usage: update.py [-h] -p PROJECT_ID [-f [FORCE]] [-u [UPDATE_STATE]] token

update existing training repo for 90COS members

positional arguments:
  token                 gitlab private token of account with 90COS access to member training repo group

optional arguments:
  -h, --help            show this help message and exit
  -p PROJECT_ID, --project-id PROJECT_ID
                        specify project-id for the member training repo project.
  -f [FORCE], --force [FORCE]
                        force update all issues in the project
  -u [UPDATE_STATE], --update-state [UPDATE_STATE]
                        add status::peer-review to upated "closed" issues
```
#### Update Examples

Update a training record repo. This will scrape issues and update if changes have happened.
```sh
./training_record.py update -p 22110838 [GitLab ACCESS TOKEN]
```

Force update all issue in a training record repo.
```sh
./training_record.py update -p 22110838 [GitLab ACCESS TOKEN] -f
```

Allow the script to change closed issues that updated to ~"status::peer-review".
```sh
./training_record.py update -p 22110838 [GitLab ACCESS TOKEN] -u
```