# Summary

- [Virtual Training Records](README.md)
- [Video: Introduction to the VTRs](promo-vid.md)
- [Apprentice Documentation](for_apprentices/README.md)
    - [How do I use this?](for_apprentices/how-to.md)
    - [Reading KSAT Tickets](for_apprentices/tickets.md)
    - [Training Video: VTR Overview](for_apprentices/overview.md)
        - [Training Video: VTR Overview (Long Version)](for_apprentices/overview_ext.md)
    - [Training Video: First Tasks](for_apprentices/first_tasks.md)
    - [Training Video: Walkthrough](for_apprentices/walkthrough.md)
    - [Course Signoff](for_apprentices/signoff.md)
- [Mentor Documentation](for_mentors/README.md)
    - [Initializing Training Repo](for_mentors/initialization.md)
    - [Training Record Issues](for_mentors/issues.md)
    - [Merge Requests](for_mentors/MRs.md)
    - [Automatic Updates](for_mentors/updates.md)
    - [Feedback Process](for_mentors/feedback.md)
- [Developer Documentation](dev_docs.md)
    
-----------

[Sample VTRs](sample-vtrs.md)  
[Video Playlist](playlist.md)
