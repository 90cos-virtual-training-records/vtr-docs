### Training Repo Issues (Virtual Training Tasks)
Each issue will have labels that give more information about the task
1. Courses that may cover the requirements for this task. 
2. Type of KSAT
3. If No eval or training are presently mapped to the task
4. The topic of the task
5. The work-role that the task is aligned with

The Issue description will have the following information
1. Description of the task
2. Acceptance Criteria; default and mentor specified definitions of done
3. Training Covered; any training material that is already mapped to this task (from the MTTL)
4. Eval Covered; any eval material that is already mapped to this task (from the MTTL)
5. (future) training reference links