## Feedback Process
```mermaid
graph LR
   a[Click Feedback Button] --> b[Submit Issue Ticket] --> c[CYT resolves Issue] --> d[Update Training Record] --> a
```

1. Click the feedback button in a KSAT issue in the training record
   - fill out a customer issue for CYT to resolve
2. CYT will resolve the issue
3. CYT will update all training records with the changes