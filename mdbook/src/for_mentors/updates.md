## Virtual Training Record Update Process
---
The act of updating your VTR involves updating all KSAT issues in the repository. This will include closing deprecated KSATs, updating issue descriptions and links, updating issue labels, and (optional) updating the state of the issue. All update information will come from the MTTL.

`Update Status` states include:
1. Deprecated KSAT: If a requirement in the MTTL no longer exists
2. Update KSAT: If the requirement information changed 
    1. Update Closed KSAT: If the information is updated when the KSAT issue is closed
    2. Update Open KSAT: If the information is updated when the KSAT issue is open

If the update script finds `Deprecated KSATs`, the script will create a merge request that will close the KSATs found when merged. This merge request will also create/prepend (the) to `DEPRECATEDLOG.md` file thereby creating a history of all KSAT issues deprecated through the update process. The merge request will automatically be assigned to the mentor (assuming the mentor created the `PRIVATE_TOKEN`) for him/her to merge.

If the update script `Updates KSATs`, the script will always update the issue `title`, `description`, training/eval covered `links`, and `labels`. If the issue had been closed before an update is made, the issue will stay closed unless the `-u` flag is set in the `UPDATE_FLAGS` environment variable in the project CI/CD variables. This flag will only effect issue who's KSAT description no longer matches that of the MTTL.

## Virtual Training Record Update Setup
---
Updating a VTR will involve updating all KSAT issues in the repository. This will include closing deprecated KSATs, updating issue descriptions and links, updating issue labels, and (optionally) updating the state of the issue (e.g. from 'Closed' to status::review). All update information will come from the MTTL.

### Generate an Access Token
The update script will require a `PRIVATE_TOKEN` for permission to handle GitLab API requests on behalf of the user. It is imperitive that the mentor be the one to generate the `PRIVATE_TOKEN` because the account will need maintainer permissions on the VTR. You can generate a `PRIVATE_TOKEN` with the following steps.
1. Make sure you're logged into GitLab
2. Go into your account settings using the dropdown menu at the top right of the GitLab view
3. Once in settings, go into `Access Tokens`
4. Type a name, an expiration date if you wish, and check on the `api` option only
5. Click `Create personal access token` and you will see your `PRIVATE_TOKEN` at the top of the screen
6. Save this token, this will be the only time you will be able to view it

### Setup Pipeline Variables
The update process utilizes GitLab CI/CD pipelines. Inside your VTR repo you will see a `.gitlab-ci.yml` but the pipeline will never trigger automatically. The script itself will need access to the `PRIVATE_TOKEN` generated previously, but because the script is run in a pipeline we will need to create a masked environment variable for the update script to use. Once this is set, you will only need to run the pipeline manually to update the VTR. You can do this with the following steps.
1. Make sure you're logged into GitLab
2. Go to the specific VTR repo you want to be able to update
3. Go into `Settings / CI/CD` and expand the `Variables` section
4. Click `Add Variable`. Set the `Key` to `PRIVATE_TOKEN`, and set the `Value` to your generated token from before
5. Make sure the `Protected variable` and `Mask variable` options are marked and click `Add variable` at the bottom right
6. You can also follow the same steps to add a `UPDATE_FLAGS` `Key` and specify `-f` and/or `-u` flags from the **Update** command section above

### Running the Pipeline to Update
Now that the update script has the `PRIVATE_TOKEN` all that is required is to manually run the pipeline to update the VTR. Alternatively, you can schedule the pipeline to run periodically. To run the pipeline manually follow these steps.
1. Make sure you're logged into GitLab
2. Go to the specific VTR repo you want to update
3. Go into `CI/CD` on the left list and click `Pipelines`
4. Click on `Run Pipeline` on the top right, and click it again once in that view.

### Scheduling Pipeline to Update automatically
To setup a scheduled pipeline to update a VTR periodically follow these steps.
1. Make sure you're logged into GitLab
2. Go to the specific VTR repo you want to update
3. Go into `CI/CD` on the left list and click `Schedules`
4. Click on `New schedule` on the top right
5. Fill out the `Description`, `Interval Pattern`, and click `Save pipeline schedule`
