## 1. Initialization Process
```mermaid
graph TD
   a[Supervisor requests workrole training in Jira] --> b[CYT generates initial VTR] --> c[Mentor and Apprentice update Definition-of-done.json] --> d[CYT Initializes full VTR] --> e{Created Successfully?} --Yes--> f[Mark Jira task as complete]
  e --No--> d 
```
1. **Supervisor** (e.g. flight commanders, site leads) submits a "Initiate Workrole Apprenticeship" ticket in Service Desk
  - Identify Apprentice - and their GitLab ID
  - Idenitfy Mentor - and their GitLab ID
  - Idenitfy Mission Workrole Training (Senior CCD, Senior PO, Master CCD, etc.)

2. **CYT** generates initial VTR with *Administrative* issues i.e. issues #1-3
  -  Updates status of training roster (currently in CVR) with new apprenticeship
    -  Update Status of Training list of apprenticeships
  -  Notify Mentor that the record is ready (Jira Ticket - Transition)  

3. **Mentor and Apprentice** complete initial admin issues 1 and 2
  - Complete working agreement
  - Defined Definition of done in definition-of-done.json
  - Mark completion in Jira Ticket
      
4. **CYT** initializes the *work-role* in the training record
  - Uses the definition of done from the repo
    
5. **Mentor and Apprentice** Verify record created successfully and begin
  - Mark the ticket complete in Service desk


__NOTE: If the Mentee is transferred to a different suborganization the record will be transferred to that suborg. Mentor may or may not change per the discretion of the losing and gaining supervisors.  The Apprentice will NOT have to re-accomplish tasks they have already completed.__