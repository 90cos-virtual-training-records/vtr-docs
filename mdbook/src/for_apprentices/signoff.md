# Course Signoff

Did you complete ACTP or IDF? When a workrole is added to your VTR, a merge request that closes issues covered by that evaluation is generated as well.

Course Signoffs are found in the merge requests section of your VTR.