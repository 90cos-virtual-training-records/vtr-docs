# How To Use The VTRs

## Accessing Issues
KSATs can be viewed in the gitlab issues section of your VTR repository. There are different ways to access issues we are mostly concerned with using lists, boards and milestones.

## List View
---
![Where to find issues](../assets/issues.png)

The list view is useful in that it lists all issues. Most likely, however, it's not the view that you will use most of the time.

Here we have an example list view listing the first three issues assigned this is discussed in the *first tasks* training video
<br><br>
![First tasks](../assets/firsttasks.png)

## Board View
---
![Board View](../assets/board.png)

The ***Board View*** is where you will do most of your work in the VTR, as it organizes issues by to-do, doing, review and closed. You should never close an issue on your own, but you should move tickets between the first three tickets. Your mentor will assign you tasks in to-do. Once you have started the task, 

<br><br>

## Milestone View
---

![Milestone View](../assets/milestone.png)

The milestone view is very useful for viewing the overarching organization of your project. Looking at the milestone view we can see that it's organized by projects. This person has been assigned the project to develop a linux remote access tool in order to transition from the cyber capabilities developer to senior cyber capabilities developer.
