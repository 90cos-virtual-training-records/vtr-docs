# Apprentice Readme

## What are VTRs?
  
**Virtual Training Records** are  version-controlled repositories, hosted by GitLab, that contains digital records of your learning. One was created when you entered the 90th, and will follow you throughout your time here. 

The VTRs provide a place to list, record and demonstrate the skills that you have acquired, as well as the skills that you still need to work on. It provides one place where you can collaborate with your mentor and how your progress can be tracked.

## Why do I have a VTR?

VTRs exist to help track a crewmember's progress through training and help them grow along the way. 

For example, a VTR can be used to become a CCD (Cyber Capabilities Developer) then an SCCD (Senior Cyber Capabilities Developer).

VTRs promote collaboration between mentor and apprentice, and allows both mentor and apprentice to discover and fix areas of growth in their workroles.

## Who can view my VTR?

Your VTRs are used by:
- You, to track your own growth and collaborate with your mentor
- Your mentor, to help you grow into your next workrole and to assign and assess training
- Your flight commander, to provide an overview of your training progress, to identify workroles that best suit you and to identify areas where your growth could benefit you and your flight
