# Reading KSAT Tickets

Because every issue matches to one KSAT, clicking on an issue will give you information about that KSAT, as well as training, eval and references that may be helpful.

![Issue 1](../assets/issue1.png)

As you complete tasks, make sure that you add ```/spend [time]``` as a comment so that you can track how long it took to achieve mastery of that particular KSAT. 

If you have feedback on the Eval, Training, or References for a particular KSAT, please submit feedback as an [issue on the mttl](https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Modify+KSAT)

Once you have completed a task to demonstrate mastery, submit your work by:
- creating a new merge request  
- completing tasks as necessary  
- submitting changes using GitLab  
- moving the ticket to Review  
- assigning the ticket to Mentor/Other Reviewer  